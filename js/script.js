"use strict";
/*
Опишіть своїми словами різницю між функціями setTimeout() і setInterval().
setTimeout() - запускає вконання функцію один раз з вказаною затримкою в часу та припиняє роботу післ її виконання.
setInterval() - запускає функцію багато разів через з вкаханим інтервалом.

Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
Код в тілі функції виконається так швидко як це можливо. Але після виконання всього скрипту, оскільки 
планувальник викликає його лише після завершення виконання поточного скрипту. 

Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?
Для скасування регуляного виконання функції, викликаної методом setInterval(). Якщо цього не зробити, то метод буже
залишатися і впливати на швидкість завантаження сторінки.
*/

const imageSection = document.querySelector(".images-wrapper");
const stopButton = document.querySelector(".stop-button");
const resumeButton = document.querySelector(".resume-button");
const timer = document.querySelector(".timer");

let remainTime = 3000;
let currentImage = 0;
let counter = remainTime;
let interval = createInterval();

function changeImage() {
    imageSection.children[currentImage].hidden = true;
    currentImage++
    if (currentImage === imageSection.children.length) {
        currentImage = 0
    }
    imageSection.children[currentImage].hidden = false;
}

function createInterval() {
    return setInterval(() => {
        counter -= 10
        if (counter <= 0) {
            counter = remainTime
            changeImage()
        }
        timer.textContent = (counter / 1000).toFixed(3)
    }, 10)
}

stopButton.addEventListener("click", () => clearInterval(interval));

resumeButton.addEventListener("click", function () {
    clearInterval(interval);
    interval = createInterval();
})

